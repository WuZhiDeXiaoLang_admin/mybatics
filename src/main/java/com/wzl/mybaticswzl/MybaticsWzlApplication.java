package com.wzl.mybaticswzl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybaticsWzlApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybaticsWzlApplication.class, args);
	}

}
